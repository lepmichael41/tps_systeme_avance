#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>

int scorePere = 0;
int scoreFils = 0;

void sig_pere(int signo)
{
	printf("Ping... P: %d\n",scorePere);
}

void sig_fils(int signo)
{
	printf("Pong... F : %d\n",scoreFils);
}

int isBallMissed()
{
	return rand() % 2;
}

void Fils()
{
	struct sigaction sa;
	sigset_t sigmask;

	sa.sa_handler = sig_fils;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	sigaction(SIGUSR2, &sa, NULL);

	while(1)
	{
		
		sleep(1);
		kill(getppid(), SIGUSR1);

		sigfillset(&sigmask);
		sigdelset(&sigmask,SIGUSR2);
		sigdelset(&sigmask,SIGINT);
		sigsuspend(&sigmask);
		
		if (scoreFils == 13)
		{
			printf("j’ai gagné %d\n",getpid());
			kill(getppid(), SIGTERM);
			exit(0);
		}
		else
		{
			scoreFils += isBallMissed();
		}
	}
}

void Pere()
{
	struct sigaction sa;
	sigset_t sigmask;
	sa.sa_handler = sig_pere;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	sigaction(SIGUSR1, &sa, NULL);

	while(1)
	{
		
		sigfillset(&sigmask);
		sigdelset(&sigmask,SIGUSR1);
		sigdelset(&sigmask,SIGINT);
		sigsuspend(&sigmask);
		sleep(1);
		kill(getpid()+1, SIGUSR2);
		if (scorePere == 13)
		{
			printf("j’ai gagné ! %d\n",getpid());
			kill(getpid()+1,SIGTERM);
			exit(EXIT_SUCCESS);
		}		
		else
		{
			scorePere += isBallMissed();
		}
	}
}


int main()
{

	if (fork() == 0)
	{
		Fils();
	}
	else
	{
		Pere();
	}

	return 0;
}