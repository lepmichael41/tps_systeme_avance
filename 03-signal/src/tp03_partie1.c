#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<signal.h>


int counter = 0;
int stop = 0;

void sig_handle_sigint(int sigint){
    counter++;
    printf("signal recu, compteur : %d\n", counter);
}

void sig_handle_sigterm(int sigterm)
{
  stop = 1;
  printf("signal de fin\n");
}

int main(int argc, char** argv) {

  sigset_t sig_proc_int;
  struct sigaction action_int;

  sigemptyset(&sig_proc_int);

  action_int.sa_mask = sig_proc_int;
  action_int.sa_flags = 0;
  action_int.sa_handler = sig_handle_sigint;

  sigaction(SIGINT, &action_int, 0);

  sigaddset(&sig_proc_int, SIGINT);
  sigprocmask(SIG_SETMASK, &sig_proc_int, NULL);

  sigset_t sig_proc_term;
  struct sigaction action_term;

  sigemptyset(&sig_proc_term);

  action_term.sa_mask = sig_proc_term;
  action_term.sa_flags = 0;
  action_term.sa_handler = sig_handle_sigterm;

  sigaction(SIGTERM, &action_term, 0);

  sigaddset(&sig_proc_term, SIGTERM);
  sigprocmask(SIG_SETMASK, &sig_proc_term, NULL);

  while (stop == 0)
  {
      
  }  
  
  return 0;
}