#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>

int main(int argc, char** argv) {

  if (fork() == 0) {
    printf("[FILS] pid :%d\n", getpid());
    printf("[PERE] pid :%d\n", getppid());
    exit(0);
  }
  else {
      wait(NULL);
    }
  
  return 0;
}