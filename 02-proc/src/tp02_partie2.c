#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<errno.h>
#include<fcntl.h>

#define STDOUT 1
#define STDERR 2

int main(int argc, char** argv) {

  if(argc < 2){
    dprintf(STDERR, "bad usage!\n");
    dprintf(STDERR, "USAGE: <string>\n");
    exit(EXIT_FAILURE);
  }
  char* string = argv[1];
  printString(string);

  if (fork() == 0) {
    printf("string will be printed in /tmp/proc-exercise\n");
    printf("[FILS] pid :%d\n", getpid());
    close(1);
    int fd = open("/tmp/proc-exercise", O_WRONLY|O_CREAT, 00777);
    int newfd = dup2(1, fd);
    printf("file fd: %d\n", newfd);
    printString(string);
    exit(0);
  }
  else {
    printf("[PERE] pid :%d\n", getpid());
    wait(NULL);
    printf("That’s All Folks !\n");
  }

  return 0;
}

void printString(char* string){
  printf("string: %s\n", string);
}