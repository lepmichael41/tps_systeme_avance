#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<errno.h>
#include<fcntl.h>

#define STDOUT 1
#define STDERR 2

int main(int argc, char** argv) {
  int tube_1[2];

  if (pipe(tube_1) < 0) {
    perror("Unable to open a pipe!");
    exit(1);
  }

  if (fork() == 0) {

    close(tube_1[0]);
    dup2(tube_1[1], 1);
    
    execlp("ps", "ps", "-eaux", NULL);
  }
  else {
    if (fork() == 0) {
      
      dup2(tube_1[0], 0);
      close(tube_1[1]);

      int fd = open("/dev/null", O_WRONLY);
      dup2(fd, 1);
      execlp("grep", "grep", "^root", NULL);
    }
    else {
      close(tube_1[0]);
      close(tube_1[1]);

      wait(NULL);
      wait(NULL);
        
      write(1,"root est connecte\n", 19);  
    }
  }

  return 0;
}